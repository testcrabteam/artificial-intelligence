;;;; 2020-09-20 14:37:06
;;;; Behold, the power of lisp!

(defun MenuHandle ()
	(defvar data ())
	(loop (PrintMenu)
		(setq guess (read))
		(cond ((eq guess 1) (setq data (CreateDatabase)))
			((eq guess 2) (setq data (AddRecord data)))
			((eq guess 3) (SaveDatabase data))
			((eq guess 4) (setq data (LoadDatabase)))
			((eq guess 5) (ShowDatabase data))
			((eq guess 6) (setq data (UpdateByNumber data)))
			((eq guess 7) (ShowInfoByNumber data))
			((eq guess 8) (MakeQuery data))
			((eq guess 9) (return))
		)
	)
)

(defun PrintMenu()
	(print "Main menu:")
	(print "	1. Create database")
	(print "	2. Add record")
	(print "	3. Save database")
	(print "	4. Load database")
	(print "	5. Show database")
	(print "	6. Update by begin point")
	(print "	7. Show info by begin point")
	(print "	8. Make query")
	(print "	9. Exit")
	(print "Enter menu number: ")
)

(defun CreateDatabase()
	(print "CreateDatabase")
)

(defun AddRecord(data)
	(print "AddRecord")
	(print "Enter start point:")
	(setq startPoint (read))
	(print "Enter end point:")
	(setq endPoint (read))
	(setq ind (+ (length data) 1))
	(acons ind (list startPoint endPoint) data)
)

(defun SaveDatabase(data)
	(print "SaveDatabase")
	(print "Enter database name:")
	(setq fileName (read))
	(setq file (open fileName :direction :output))
	(dolist (el data)
		(print el file)		
	)

	(close file)
)

(defun LoadDatabase()
	(print "LoadDatabase")
	(print "Enter database name:")
	(setq fileName (read))
	(defvar data ())

	(let ((in (open fileName :direction :input :if-does-not-exist nil)))
	   (when in
	      (loop for line = (read in nil)
	      
	      while line do (
				push line data
			)
	      )
	      (close in)
	   )
	)

	(return-from LoadDatabase data)
)

(defun ShowDatabase(data)
	(print "ShowDatabase")
	(print data)
)

(defun UpdateByNumber(data)
	(print "Enter path number:")
	(setq num (read))
	(setq record (assoc num data))

	(print "Enter start point:")
	(setq startPoint (read))
	(print "Enter end point:")
	(setq endPoint (read))
	(rplacd record (list startPoint endPoint))
	(return-from UpdateByNumber data)
)	

(defun ShowInfoByNumber(data)
	(print "Enter path number:")
	(setq num (read))
	(setq record (assoc num data))
	(if record (print record) (print "NO"))
)

(defun MakeQuery(data)
	(print "Print your query here: ")
	(setq guess (read))

	(cond 

		((match '(* record with number ?Id) guess)
		(print (GetMatches (list ?Id '? '?) data)))

		((match '(* records with start point ?num) guess)
		(print (GetMatches (list '? ?num '?) data)))

		((match '(* records with end point ?num) guess)
		(print (GetMatches (list '? '? ?num) data)))

		(t (print "Query cannot be parsed"))
	)
)

;
(defun GetMatches (p db)
	;db - база данных
	;p - запрос (образец)
	(cond ((null db) nil)
		((match p (car db))
		(cons (car db) (GetMatches p (cdr db))))
		(t (GetMatches p (cdr db)))
	)
)

(DEFUN match(P D)
  (COND ( (AND (NULL P) (NULL D))  T  )
        ( (OR  (NULL P) (NULL D)) NIL )
        ( (OR (EQ (CAR P) '?) (EQ (CAR P) (CAR D)))
              (match (CDR P) (CDR D)) )
        ( (AND (ATOM (CAR P)) (IsVariable (car p))
               (match (CDR P) (CDR D)))
               (SET (CAR P) (CAR D)) T )
        ( (EQ (CAR P) '*)
              (COND ( (match (CDR P) (CDR D)) )
                    ( (match P (CDR D))) ) )
  )
)

(DEFUN IsVariable(str)
	(setq atomStr (format NIL "~a" str))
 	(EQUAL (subseq atomStr 0 1) "?")
)

(MenuHandle)
